#########################################################
##  CS 4750 (Fall 2018), Assignment #1, Question #1    ##
##   Script File Name: tcomp1.py                       ##
##       Student Name: Devin Marsh                     ##
##         Login Name: dfcm15                          ##
##              MUN #: 201239464                       ##
#########################################################

"""

"""

# Imports
import sys

# Constants
MIN_ARGS = 4
SUM_KEY = "_sum_"
NAME_KEY = "_name_"
SCORE_KEY = "_score_"
NEWLINE_CHAR = "\n"

"""FORWARD DECLARE FUNCTIONS"""

"""
Read in file and calculate the N-grams frequencies. Returns a dictionary such that key = N-Gram, and value = frequency percentage. Special key "_sum_" is the number of N-Grams in file
"""
def calculateNGrams(filename, n):
    dict = {}

    with open(filename) as file:

        # Read lines from file
        for line in file:
            # Continue if empty line
            if (not line) or (line == NEWLINE_CHAR):
                continue

            # Iterate over words
            words = line.split(" ")
            for word in words:
                # Continue if empty word
                if (not word) or (word == ""): continue

                # Continue if word has less characters than required for N-Gram
                if len(word) < n: continue

                # Iterate over the word, adding new N-Grams as we process
                for i in range(len(word)-1):
                    _perm = word[i:i+n]

                    if NEWLINE_CHAR in _perm:
                        continue

                    if _perm in dict:
                        dict[_perm] = dict[_perm] + 1
                    else:
                        dict[_perm] = 1

    # Calculate the number of N-Grams in the current file
    for value in dict.values():
        if SUM_KEY not in dict:
            dict[SUM_KEY] = 0
        dict[SUM_KEY] = dict[SUM_KEY] + value

    # Calculate the frequency percentage of N-Grams in the current file
    for key in dict:
        if key == SUM_KEY: continue

        dict[key] = float(dict[key]) / float(dict[SUM_KEY])

    return dict

"""
Calculate similarity scores. Returns the sum of N-Gram frequency differences between masterDict and objDict
"""
def calculateSimScores(masterDict, objDict):
    diffSum = 0.0
    for key in masterDict:
        if key == SUM_KEY: continue

        if key in objDict:
            diffSum = diffSum + abs(masterDict[key] - objDict[key])

    if diffSum == 0.0: return diffSum
    else: return 1.0 - (diffSum / 2.0)

"""BEGIN PROGRAM"""

args = sys.argv

# Warn user if there is less than required number of args
if len(args) < MIN_ARGS:
    sys.exit("Missing program arguments. Must have a master file, n, and two or more comparison files")

# Parse arguments
master = args[1]  # 1 because 0 is the program name
n = int(args[2])
filenames = args[3:len(args)]
filenames.insert(0, master)

# Calculate N-Grams and store in a dictionary
nGrams = {}
for filename in filenames:
    nGrams[filename] = calculateNGrams(filename, n)

# Calculate similarity scores
simScores = {}
for filename in filenames[1:]:
    simScores[filename] = calculateSimScores(nGrams[master], nGrams[filename])

# Variables to determine most similar file
mostSim = {}
mostSim[SCORE_KEY] = 0.0

# Print scores
print "Simularity Scores (n = %d):" % (n)
for key in simScores:
    print "- '%s' compared to '%s': %f%%" % (master, key, simScores[key] * 100)

    # Determine most similar file
    if simScores[key] >= mostSim[SCORE_KEY]:
        mostSim[SCORE_KEY] = simScores[key]
        mostSim[NAME_KEY] = key

print "Most similar file to master file '%s' is '%s' with %f%%" % (master, mostSim[NAME_KEY], mostSim[SCORE_KEY] * 100)