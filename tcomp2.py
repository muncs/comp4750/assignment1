#########################################################
##  CS 4750 (Fall 2018), Assignment #1, Question #2    ##
##   Script File Name: tcomp1.py                       ##
##       Student Name: Devin Marsh                     ##
##         Login Name: dfcm15                          ##
##              MUN #: 201239464                       ##
#########################################################

"""
Given two text files, let the similarity score between them be Sim(X, Y) = 1.0 - (SD(X, Y) / (nW(X) + nW(Y))) where:

SD(X, Y) is the systemetric difference of unique words in both textfiles
nW(X) is the total number of words in textfile X

NOTE: 'Note that nW() does not count the total number of words in a file, but rather the number of different words that occur in a file.':

I found that if I let nW() count the number of unique words per file, I would get slightly different results than tcomp2.script.
Instead, I changed it to count the total number of words in a file, and now I get the exact results from tcomp2.script.
"""

# Imports
import sys
from sets import Set

# Constants
MIN_ARGS = 3
SCORE_KEY = "_score_"
SET_KEY = "_set_"
NAME_KEY = "_name_"
SIZE_KEY = "_size_"
NEWLINE_CHAR = "\n"

"""FORWARD DECLARE FUNCTIONS"""

"""
Read in words from a file and store them into a set
"""
def populateWords(dict, filename):
    wordSet = Set([])

    with open(filename) as file:

        # Read lines from file
        for line in file:
            # Continue if empty line
            if (not line) or (line == "\n"):
                continue

            # Iterate over words
            words = line.split(" ")
            for word in words:
               # Trim word if it includes newline character
                if NEWLINE_CHAR in word:
                    word = word[:len(word) - len(NEWLINE_CHAR)]

                # Skip if whitespace
                if word:
                    wordSet.add(word)
                    dict[SIZE_KEY] += 1

    dict[SET_KEY] = wordSet
    return dict

def calculateSimScore(master, obj):
    # Use set sysmetric difference to calculate the number of unique words in master that are not in obj, and vice versa
    numUniques = len(master[SET_KEY].symmetric_difference(obj[SET_KEY]))

    obj[SCORE_KEY] = 1.0 - (numUniques / (float(master[SIZE_KEY]) + float(obj[SIZE_KEY])))

    return

"""BEGIN PROGRAM"""
args = sys.argv

# Warn user if there is less than required number of args
if len(args) < MIN_ARGS:
    sys.exit("Missing program arguments. Must have a master file, and two or more comparison files")

# Parse arguments
master = args[1]  # 1 because 0 is the program name
filenames = args[2:len(args)]
filenames.insert(0, master)

# Get the word sets for each file and store them in a common set
fileData = {}
for filename in filenames:
    obj = {SCORE_KEY: 0.0, SIZE_KEY: 0}
    fileData[filename] = populateWords(obj, filename)

# Calculate similarity score
for filename in filenames[1:]:
    calculateSimScore(fileData[master], fileData[filename])

# Print scores
mostSim = {SCORE_KEY: 0.0}
print "Simularity Scores:"
for _file in fileData:
    if _file == master: continue    # Skip master file
    print "- '%s' compared to '%s': %f%%" % (master, _file, fileData[_file][SCORE_KEY] * 100)

    # Determine most similar file
    if fileData[_file][SCORE_KEY] >= mostSim[SCORE_KEY]:
        mostSim[SCORE_KEY] = fileData[_file][SCORE_KEY]
        mostSim[NAME_KEY] = _file
#
print "Most similar file to master file '%s' is '%s' with %f%%" % (master, mostSim[NAME_KEY], mostSim[SCORE_KEY] * 100)